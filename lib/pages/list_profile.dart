import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_image_slideshow/flutter_image_slideshow.dart';
import 'package:project_note/database/database_helper.dart';
import 'package:project_note/model/profile_model.dart';
import 'package:project_note/pages/about.dart';
import 'package:project_note/pages/add_profile.dart';
import 'package:project_note/pages/edit_profile.dart';
import 'package:project_note/pages/show_profile.dart';

class ListProfile extends StatefulWidget {
  const ListProfile({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<ListProfile> createState() => _ListProfileState();
}

class _ListProfileState extends State<ListProfile> {
  int? selectedId;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: Column(
          children: <Widget>[
            ImageSlideshow(
              width: double.infinity,

              height: 300,

              initialPage: 0,

              indicatorColor: Colors.blue,

              indicatorBackgroundColor: Colors.grey,

              /// Called whenever the page in the center of the viewport changes.
              onPageChanged: (value) {
                print('Page changed: $value');
              },

              /// Auto scroll interval.
              /// Do not auto scroll with null or 0.
              autoPlayInterval: 3000,

              /// Loops back to first slide.
              isLoop: true,

              children: [
                Image.asset(
                  'assets/images/293605419_1302085963531143_1832884766204330258_n.jpg',
                  fit: BoxFit.cover,
                ),
                Image.asset(
                  'assets/images/294852832_619484906412637_7857690895429842377_n.jpg',
                  fit: BoxFit.cover,
                ),
                Image.asset(
                    'assets/images/301381527_3261455584170803_4536838190935692170_n.jpg',
                    fit: BoxFit.cover),
                Image.asset(
                  'assets/images/303007166_1102971147003714_5894471478928523712_n.jpg',
                  fit: BoxFit.cover,
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(),
              child: ListTile(
                leading: Padding(
                  padding: EdgeInsets.all(8),
                  child: Icon(
                    Icons.menu_book,
                    size: 50,
                  ),
                ),
                title: Padding(
                  padding: EdgeInsets.all(8),
                  child: Text(
                    'Add Menu',
                    style: TextStyle(
                      fontSize: 25,
                    ),
                  ),
                ),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return AddProfile();
                  }));
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.only(),
              child: ListTile(
                leading: Padding(
                  padding: EdgeInsets.all(8),
                  child: Icon(
                    Icons.people,
                    size: 50,
                  ),
                ),
                title: Padding(
                  padding: EdgeInsets.all(8),
                  child: Text(
                    'About Us',
                    style: TextStyle(
                      fontSize: 25,
                    ),
                  ),
                ),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return About();
                  }));
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.only(),
              child: ListTile(
                leading: Padding(
                  padding: EdgeInsets.all(8),
                  child: Icon(
                    Icons.restaurant_menu,
                    size: 50,
                  ),
                ),
                title: Padding(
                  padding: EdgeInsets.all(8),
                  child: Text(
                    'Menu',
                    style: TextStyle(
                      fontSize: 25,
                    ),
                  ),
                ),
                onTap: (){

                },
              ),
            ),
          ],
        ),
      ),
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          Padding(
            padding: const EdgeInsets.only(top: 10.0, left: 15.0, right: 15.0,bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Builder(builder: (context) {
                  return Row(
                    children: [
                      Image.asset('assets/images/logoggg.jpg',height: 100,),
                    ],
                  );
                }),
              ],
            ),
          )
        ],
      ),
      body: Center(
        child: FutureBuilder<List<ProfileModel>>(
            future: DatabaseHelper.instance.getProfiles(),
            builder: (BuildContext context,
                AsyncSnapshot<List<ProfileModel>> snapshot) {
              if (!snapshot.hasData) {
                return Center(child: Text('Loading...'));
              }
              return snapshot.data!.isEmpty
                  ? Center(child: Text('No Profiles in List.'))
                  : ListView(
                      children: snapshot.data!.map((grocery) {
                        return Center(
                          child: Card(
                            color: selectedId == grocery.id
                                ? Colors.white70
                                : Colors.white,
                            child: ListTile(
                              title: Text('${grocery.firstname} '),
                              subtitle: Text('${grocery.lastname}'),
                              //subtitle: Text(grocery.email),
                              leading: CircleAvatar(
                                  backgroundImage:
                                      FileImage(File(grocery.image))),
                              //leading: Image(
                              //  image: FileImage(File(grocery.image)),
                              //  fit: BoxFit.cover,
                              //  height: 500,
                              //  width: 100,
                              //),
                              trailing: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  new IconButton(
                                    padding: EdgeInsets.all(0),
                                    icon: Icon(Icons.edit),
                                    onPressed: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                EditProfile(grocery)),
                                      ).then((value) {
                                        setState(() {});
                                      });
                                    },
                                  ),
                                  new IconButton(
                                    padding: EdgeInsets.all(0),
                                    icon: Icon(Icons.clear),
                                    onPressed: () {
                                      showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return AlertDialog(
                                            title: new Text(
                                                "Do you want to delete this record?"),
                                            // content: new Text("Please Confirm"),
                                            actions: [
                                              new TextButton(
                                                onPressed: () {
                                                  DatabaseHelper.instance
                                                      .remove(grocery.id!);
                                                  setState(() {
                                                    Navigator.of(context).pop();
                                                  });
                                                },
                                                child: new Text("Ok"),
                                              ),
                                              Visibility(
                                                visible: true,
                                                child: new TextButton(
                                                  onPressed: () {
                                                    Navigator.of(context).pop();
                                                  },
                                                  child: new Text("Cancel"),
                                                ),
                                              ),
                                            ],
                                          );
                                        },
                                      );
                                    },
                                  ),
                                ],
                              ),
                              onTap: () {
                                var profileid = grocery.id;
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            ShowProfile(id: profileid)));

                                setState(() {
                                  print(grocery.image);
                                  if (selectedId == null) {
                                    //firstname.text = grocery.firstname;
                                    selectedId = grocery.id;
                                  } else {
                                    // textController.text = '';
                                    selectedId = null;
                                  }
                                });
                              },
                              onLongPress: () {
                                setState(() {
                                  DatabaseHelper.instance.remove(grocery.id!);
                                });
                              },
                            ),
                          ),
                        );
                      }).toList(),
                    );
            }),
      ),
    );
  }
}
