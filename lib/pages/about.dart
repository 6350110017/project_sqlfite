import 'package:flutter/material.dart';

class About extends StatelessWidget {
  const About({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('About Us'),
        backgroundColor: Colors.pinkAccent,
      ),
      body: Container(
        constraints: BoxConstraints.expand(),
        child: SingleChildScrollView(
          child: Column(
            children: const [
              CircleAvatar(
                radius: 100,
                backgroundImage: AssetImage('assets/images/imgPro1.jpg',)
              ),
              Text('6350110017 นายศรายุธ สิทธิเดข ICM',style: TextStyle(fontSize: 20),),
              SizedBox(height: 50,),
              CircleAvatar(
                radius: 100,
                backgroundImage: AssetImage('assets/images/imgPro2.jpg'),
              ),
              Text('6350110022 นางสาวอรอินทุ์ หวันสู ICM',style: TextStyle(fontSize: 20),),
              SizedBox(height: 50,),
              Image(image: AssetImage('assets/images/goodbye-bye-bye.gif',),height: 100,)
            ],
          ),
        )
      ),
    );
  }
}
